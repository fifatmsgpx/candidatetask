
# README #

Create a service that fetches content from Wikipedia by using [Wikipedia query API](https://www.mediawiki.org/wiki/API:Query).

Finish as many tasks as your time enables.

Create a branch with your name (`firstName_lastName`) and commit your code there.

Don't worry if you can't hit the bottom of the tasks list, the focus is not on full task completion.

### How much time do you have? ###

* You have 3 hours to work on the following tasks

### Do you need more Apps/Tools for development?

* Feel free to install any additional apps/tools that you need ( but not viruses please :-) )

### Your Tasks ###

* Create a service with an endpoint `GET /article?articleTitle={titleOfTheArticle}` that accepts one query parameter: `articleTitle`, the title of a Wikipedia Article.
    * The defined endpoint should make a Wikipedia API query and return the content of the queried article if there was a result
        * To search for the article with WikiAPI, you can use the following endpoint: https://en.wikipedia.org/w/api.php?action=query&titles=Windsurfing&prop=revisions&rvprop=content&format=json&formatversion=2
        * You can use this endpoint to check the structure of the response: https://en.wikipedia.org/w/api.php?action=query&titles=Windsurfing&prop=revisions&rvprop=content&format=jsonfm&formatversion=2
    * When making a query, also store the results in RavenDB
    * If a request is made to fetch an article that has been previously fetched and stored in RavenDB, get the data directly from RavenDB without calling the Wikipedia API
    * The content returned by Wikipedia API is not very tidy ([Wiki Markup Language](https://en.wikipedia.org/wiki/Help:Wikitext)), so you have to clean it the following way:
        * the cleaned content should be a space separated list of tokens (i.e. no new lines in the cleaned text)
        * keep only those tokens that are containing letters and/or numbers
    * The returning json object should have the following format:


```
    
        {
    
            articleTitle: string, // the article title that we were searching for
        
            pageContent: string, // the cleansed content of the first found wikipedia article if there is any
        
            wordStatistics: IEnumerable<KeyValuePair<string, int>> // word occurrence statistics <word, numberOfOccurrence in article>, in descending order by the occurrence number
        
        }
    
```

* Create a statistics endpoint `GET /article/stat?articleTitle={titleOfTheArticle}` that
    * accepts 1 query parameter: `articleTitle` 
    * returns only the word statistics (querying only from the Database, doesn't have to call WikiAPI) for the given article name if exists, ordered descending by the occurrences of the words
    * returns the following json object:

```
    
        {

            articleTitle: string,
      
            wordStatistics: IEnumerable<KeyValuePair<string, int>> // word occurrence statistics of the first found article if there is any

        }

```

* Write tests if you have time!

### In case you need help ###

* There are some examples in the code already, you can use them to solve your task.

* Don't hesitate to ask us if you stuck on something or just have some questions about the tasks
