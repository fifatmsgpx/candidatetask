﻿using System.Collections.Generic;
using ServiceStack;

namespace WebApplication1.ServiceModel
{
    [Route("/hello/{Name}")]
    public class Hello : IReturn<HelloResponse>
    {
        public string Name { get; set; }
    }

    public class HelloResponse
    {
        public string Result { get; set; }
    }

    [Route("/getStats")]
    public class GetStats : IReturn<GetStatsResponse>
    {
        
    }

    public class GetStatsResponse
    {
        public List<string>  Results { get; set; }

        public GetStatsResponse()
        {
            Results = new List<string>();
        }
    }
}