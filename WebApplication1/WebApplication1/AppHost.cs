﻿using System.Collections.Generic;
using Funq;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using ServiceStack;
using WebApplication1.Index;
using WebApplication1.Services;

namespace WebApplication1
{
    //VS.NET Template Info: https://servicestack.net/vs-templates/EmptyAspNet
    public class AppHost : AppHostBase
    {
        /// <summary>
        /// Base constructor requires a Name and Assembly where web service implementation is located
        /// </summary>
        public AppHost()
            : base("WebApplication1", typeof(MyServices).Assembly) { }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        public override void Configure(Container container)
        {
            //Config examples
            //this.Plugins.Add(new PostmanFeature());
            //this.Plugins.Add(new CorsFeature());

            var db = new DocumentStore
            {
                DefaultDatabase = "TaskDatabase",
                Url = "http://localhost:8080"
            }.Initialize();

            db.ExecuteIndexes(new List<AbstractIndexCreationTask>
            {
                new Vehicle_Index()
            });

            container.Register<IDocumentStore>(db);
        }
    }
}