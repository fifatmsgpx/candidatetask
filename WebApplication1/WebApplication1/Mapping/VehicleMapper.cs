﻿using System.Collections.Generic;
using System.Linq;
using WebApplication1.Model;
using WebApplication1.ServiceModel;

namespace WebApplication1.Mapping
{
    public static class VehicleMapper
    {
        
        public static GetStatsResponse ToGetStatsResponse(this List<Vehicle> vehicleList)
        {
            return new GetStatsResponse
            {
                Results = vehicleList.Select(v => v.Name).ToList()
            };
        }

    }
}