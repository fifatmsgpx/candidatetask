﻿namespace WebApplication1.Model
{
    public class Vehicle
    {
        public string Name { get; set; }
        public int Wheels { get; set; }
    }
}
