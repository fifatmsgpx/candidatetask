﻿using System.Linq;
using Raven.Client;
using ServiceStack;
using WebApplication1.Index;
using WebApplication1.Model;
using WebApplication1.ServiceModel;
using WebApplication1.Mapping;

namespace WebApplication1.Services
{
    public class MyServices : Service
    {
        private readonly IDocumentStore _documentStore;

        public MyServices(IDocumentStore documentStore)
        {
            this._documentStore = documentStore;
        }

        public object Get(Hello request)
        {
            using (var session = _documentStore.OpenSession())
            {
                session.Store(new Vehicle { Name = request.Name});

                session.SaveChanges();
            }

            return new HelloResponse { Result = $"Hello, {request.Name}!" };
        }

        public object Get(GetStats request)
        {
            using (var session = _documentStore.OpenSession())
            {
                var result = session.Query<Vehicle, Vehicle_Index>().ToList();

                return result.ToGetStatsResponse();
            }
        }
    }
}