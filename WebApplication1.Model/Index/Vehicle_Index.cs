﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace WebApplication1.Model.Index
{
    public class Vehicle_Index : AbstractIndexCreationTask<Vehicle, Vehicle_Index.IndexEntry>
    {
        public class IndexEntry
        {
            public string Name { get; set; }
        }

        public Vehicle_Index()
        {
            Map = vehicles => from vehicle in vehicles
                               select new IndexEntry
                               {
                                   Name = vehicle.Name
                               };


            Index(e => e.Name, FieldIndexing.Analyzed);
        }
    }
}
